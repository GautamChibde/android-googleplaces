package com.example.infiny.mapapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Data implements Parcelable {
    private String next_page_token;
    private List<Results> results;

    public Data() {
    }

    protected Data(Parcel in) {
        next_page_token = in.readString();
        this.results = in.createTypedArrayList(Results.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(next_page_token);
        dest.writeTypedList(results);
    }

    public static final Creator<Data> CREATOR = new Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };

    public String getNext_page_token() {
        return next_page_token;
    }

    @Override
    public String toString() {
        return "Data{" +
                "next_page_token='" + next_page_token + '\'' +
                ", results=" + results +
                '}';
    }

    public void setNext_page_token(String next_page_token) {
        this.next_page_token = next_page_token;
    }

    public List<Results> getResults() {
        if (results == null) {
            return new ArrayList<>();
        }
        return results;
    }

    public void setResults(List<Results> results) {
        this.results = results;
    }
}
