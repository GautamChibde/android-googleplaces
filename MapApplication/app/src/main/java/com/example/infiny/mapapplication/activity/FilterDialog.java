package com.example.infiny.mapapplication.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.infiny.mapapplication.R;

import de.greenrobot.event.EventBus;

import com.gts.ui.view.fragment.AbstractDialogFragment;

public class FilterDialog extends AbstractDialogFragment {
    private TextView textView;
    private int progressValue;
    private Spinner mSpinner;
    private String placeSelected;
    private int pos;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.filter_dialog, container, false);
        setupToolbar(
                (Toolbar) v.findViewById(R.id.toolbar),
                "Filter",
                R.drawable.ic_arrow_back_black_24dp,
                R.menu.menu_done,
                R.id.tvTitle,
                new Toolbar.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.actionDone:
                                dismiss();
                                return true;
                        }
                        return false;
                    }
                }
        );
        initView(v);
        return v;
    }

    private void initView(View v) {
        mSpinner = (Spinner) v.findViewById(R.id.sprPlaces);
        SeekBar seekBar = (SeekBar) v.findViewById(R.id.seekBar);
        textView = (TextView) v.findViewById(R.id.tvSeek);
        initSpinner();
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressValue = progress * 1000;
                textView.setText(String.valueOf("Within : " + (progressValue + 4000) + " mt. "));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                textView.setText(String.valueOf("Within : " + (progressValue + 4000) + " mt."));

            }
        });
        textView.setText(String.valueOf("Within : " + "4000 mt."));
        v.findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new FilterValues(placeSelected, progressValue));
                dismiss();
            }
        });
    }

    private void initSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.places_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String[] placeTypes = getResources().getStringArray(R.array.place_type);
                pos = mSpinner.getSelectedItemPosition();
                placeSelected = placeTypes[pos];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public static class FilterValues {
        private String selectedPlaces;
        private int progressValue;

        public FilterValues(String selectedPlaces, int progressValue) {
            this.selectedPlaces = selectedPlaces;
            this.progressValue = progressValue;
        }

        @Override
        public String toString() {
            return "FilterValues{" +
                    "selectedPlaces='" + selectedPlaces + '\'' +
                    ", progressValue=" + progressValue +
                    '}';
        }

        public String getSelectedPlaces() {
            return selectedPlaces;
        }

        public int getProgressValue() {
            return progressValue;
        }
    }
}
