package com.example.infiny.mapapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

public class Results implements Parcelable {

    private Geometry geometry;
    private String formatted_address;
    private String icon;
    private String id;
    private String name;
    private List<String> types;

    public Results() {
    }

    protected Results(Parcel in) {
        geometry = in.readParcelable(Geometry.class.getClassLoader());
        formatted_address = in.readString();
        icon = in.readString();
        id = in.readString();
        name = in.readString();
        types = in.createStringArrayList();
    }

    public static final Creator<Results> CREATOR = new Creator<Results>() {
        @Override
        public Results createFromParcel(Parcel in) {
            return new Results(in);
        }

        @Override
        public Results[] newArray(int size) {
            return new Results[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(geometry, flags);
        dest.writeString(formatted_address);
        dest.writeString(icon);
        dest.writeString(id);
        dest.writeString(name);
        dest.writeStringList(types);
    }

    @Override
    public String toString() {
        return "Results{" +
                "geometry=" + geometry +
                ", formatted_address='" + formatted_address + '\'' +
                ", icon='" + icon + '\'' +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", types=" + types +
                '}';
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
