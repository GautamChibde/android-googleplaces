package com.example.infiny.mapapplication.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;

import com.example.infiny.mapapplication.MapApp;
import com.example.infiny.mapapplication.R;
import com.example.infiny.mapapplication.model.Data;
import com.example.infiny.mapapplication.model.Items;
import com.example.infiny.mapapplication.model.Results;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.gson.reflect.TypeToken;
import com.google.maps.android.clustering.ClusterManager;
import com.gts.infra.net.http.Resource;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused,unchecked")
public class MapsActivity extends BaseMapActivity {
    private List<Results> markers = new ArrayList<>();
    private int progressValue = 4000;
    private String placeSelected;
    private ClusterManager<Items> mClusterManager;
    private String nextPageTokens;

    private void requestServer(String url) {
        progress.show();
        Resource.getAsync(
                getApp(),
                url,
                200,
                new TypeToken<Data>() {
                }.getType(),
                "data");
    }

    public void onEvent(FilterDialog.FilterValues event) {
        progress.dismiss();
        markers.clear();
        getMap().clear();
        placeSelected = event.getSelectedPlaces();
        progressValue = event.getProgressValue();
        requestServer(getUrl(placeSelected));
    }

    public void onEvent(Resource.HttpRequestComplete event) {
        if (event.isSuccessful()) {
            Data data = (Data) event.getResponse();
            if (data.getNext_page_token() != null) {
                nextPageTokens = data.getNext_page_token();
            }
            if (markers.isEmpty()) {
                markers = data.getResults();
            } else {
                markers.addAll(data.getResults());
            }
            if (markers.size() < 40) {
                requestServer(getUrlNextPage());
            }
            setUpCluster();
            progress.dismiss();
        } else {
            showDialog("Problem with your network");
        }
        progress.dismiss();
    }

    private void showDialog(String message) {
        new AlertDialog.Builder(this)
                .setTitle("network error")
                .setMessage(message)
                .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        requestServer(getUrl(placeSelected));
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.ic_pause_dark)
                .setIconAttribute(android.R.attr.alertDialogIcon)
                .show();
    }

    @Override
    protected int getMapId() {
        return R.id.map;
    }

    @Override
    protected void init() {
        findViewById(R.id.fabFilter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMap().clear();
                FilterDialog dialog = new FilterDialog();
                dialog.show(getFragmentManager(), "filter dialog");
            }
        });
        getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(getCurrentLocation(), 12));
        requestServer(getUrl("atm|bakery|bus_station|gym|gas_station|" +
                "pet_store|train_station|restaurant|shopping_mall|" +
                "taxi_stand|movie_theater"));
        setTitle(R.string.app_name);
    }

    private void setUpCluster() {
        mClusterManager = new ClusterManager<>(this, getMap());
        getMap().setOnCameraIdleListener(mClusterManager);
        getMap().setOnMarkerClickListener(mClusterManager);
        addItems();
    }

    private void addItems() {
        List<Items> items = new ArrayList<>();
        for (Results r : markers) {
            items.add(new Items(r.getGeometry().getLocation().getLat(),
                    r.getGeometry().getLocation().getLng()));
        }
        mClusterManager.addItems(items);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_maps;
    }

    public String getUrl(String placeSelected) {
        return "https://maps.googleapis.com/maps/api/place/nearbysearch/json?" +
                "location=" + getCurrentLocation().latitude + "," + getCurrentLocation().longitude +
                "&radius=" + progressValue +
                "&types=" + placeSelected +
                "&sensor=true" +
                "&key=" + getResources().getString(R.string.google_maps_key);
    }

    public String getUrlNextPage() {
        return "https://maps.googleapis.com/maps/api/place/nearbysearch/json?" +
                "location=" + getCurrentLocation().latitude + "," + getCurrentLocation().longitude +
                "&radius=" + progressValue +
                "&sensor=false" +
                "&key=" + getResources().getString(R.string.google_maps_key) +
                "&pagetoken=" + nextPageTokens;
    }

    public BitmapDescriptor getIcon(List<String> s) {
        for (String a : s) {
            if (MapApp.getIcons().containsKey(a)) {
                return BitmapDescriptorFactory.fromResource(MapApp.getIcons().get(a));
            }
        }
        return BitmapDescriptorFactory.fromResource(R.drawable.busstop);
    }

    @Override
    protected int getTitleId() {
        return R.id.tvTitle;
    }

    @Override
    protected int getToolbarId() {
        return R.id.toolbar;
    }
}
