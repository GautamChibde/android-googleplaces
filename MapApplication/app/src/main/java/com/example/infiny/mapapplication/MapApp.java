package com.example.infiny.mapapplication;

import com.gts.infra.App;

import java.util.HashMap;

public class MapApp extends App {
    private static final HashMap<String,Integer> icons = new HashMap<>();

    @Override
    public void onCreate() {
        super.onCreate();
        icons.put("atm",R.drawable.atm);
        icons.put("bakery",R.drawable.bread);
        icons.put("bus_station",R.drawable.busstop);
        icons.put("gym",R.drawable.fitness);
        icons.put("gas_station",R.drawable.fillingstation);
        icons.put("pet_store",R.drawable.pets);
        icons.put("restaurant",R.drawable.restaurant_indian);
        icons.put("shopping_mall",R.drawable.mall);
        icons.put("taxi_stand",R.drawable.taxi);
        icons.put("movie_theater",R.drawable.congress);
        initHttpQueue();
    }

    public static HashMap<String, Integer> getIcons() {
        return icons;
    }

    public String getBaseUri() {
        return "";
    }
}
