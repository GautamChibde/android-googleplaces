package com.gts.infra.data;

import android.support.annotation.NonNull;

import com.gts.util.Constant;
import com.gts.util.TextUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SearchRequest {

    protected String doc;
    protected String selection;
    protected Date updatedStamp;
    protected String sort;


    public SearchRequest() {
    }

    public SearchRequest(String doc) {
        this.doc = doc;
    }

    public SearchRequest(String doc, String selection, String sort) {
        this.doc = doc;
        this.selection = selection;
        this.sort = sort;
    }

    public SearchRequest(String doc, Date updatedStamp, String sort) {
        this.doc = doc;
        this.updatedStamp = updatedStamp;
        this.sort = sort;
    }

    public SearchRequest(String doc, Date updatedStamp) {
        this.doc = doc;
        this.updatedStamp = updatedStamp;
    }

    @Override
    public String toString() {
        return "SearchRequest{" +
                "doc='" + doc + '\'' +
                ", selection='" + selection + '\'' +
                ", sort='" + sort + '\'' +
                '}';
    }

    public static SearchRequest getUpdatedSinceRequest(String doc, Date lastUpdated) {
        return new SearchRequest(
                doc,
                lastUpdated == null ? null : getLastUpdatedWhereClause(lastUpdated),
                null);
    }

    @NonNull
    private static String getLastUpdatedWhereClause(Date lastUpdated) {
        return "{updated_stamp > '" +
                TextUtils.quoteString(
                        new SimpleDateFormat(
                                Constant.DATE_PATTERN,
                                Locale.getDefault()).format(lastUpdated))
                + "'}";
    }

    public Date getUpdatedStamp() {
        return updatedStamp;
    }

    public void setUpdatedStamp(Date updatedStamp) {
        this.updatedStamp = updatedStamp;
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }
}
