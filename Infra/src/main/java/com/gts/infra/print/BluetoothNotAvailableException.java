package com.gts.infra.print;

public class BluetoothNotAvailableException extends BluetoothPrinterException {
    public BluetoothNotAvailableException(String s) {
        super(s);
    }
}
