package com.gts.infra.print;

public class NotPairedException extends BluetoothPrinterException {

    public NotPairedException(String s) {
        super(s);
    }

}

