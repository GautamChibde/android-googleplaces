package com.gts.infra.print;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/*
    TODO :  Communicate status with the calling activity
 */
public class BluetoothPrinterImpl implements BluetoothPrinter {

    private static final String TAG = BluetoothPrinterImpl.class.getName();
    private static final String SPP_UUID = "00001101-0000-1000-8000-00805F9B34FB";
    private final int CONNECT_RETRIES = 2;

    private final Context context;
    private final BluetoothAdapter mBluetoothAdapter;
    private final String printerName;


    private String message;

    public BluetoothPrinterImpl(Context context, String printerName) {
        this.context = context;
        this.printerName = printerName;
        this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    @Override
    public void print(String message) {
        this.message = message;
        connectAndPrint();
    }

    private void connectAndPrint() {
        checkBluetooth();
        enableBluetooth();
        new PrintTask(printerName, mBluetoothAdapter).execute();
    }

    private void disconnect() {
        disableBluetooth();
        Log.i(TAG, "Done");
    }

    private void checkBluetooth() {
        if (mBluetoothAdapter == null) {
            throw new BluetoothNotAvailableException("Device does not have bluetooth");
        }
    }

    private void enableBluetooth() {
        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }
    }

    private void disableBluetooth() {
        if (mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.disable();
        }
    }

    private class PrintTask extends AsyncTask<Void, Integer, Integer> {

        private final String printerName;
        private final BluetoothAdapter mBluetoothAdapter;
        private final int PRINT_RETRIES = 5;
        private final AtomicInteger print_retry_counter = new AtomicInteger(0);

        public PrintTask(String name, BluetoothAdapter mBluetoothAdapter) {
            this.printerName = name;
            this.mBluetoothAdapter = mBluetoothAdapter;
        }

        protected Integer doInBackground(Void... noData) {
            while (print_retry_counter.get() < PRINT_RETRIES) {
                try {
                    Log.i(TAG, "printing message.");
                    if (printMessage()) {
                        print_retry_counter.set(0);
                        return 0;
                    } else {
                        Log.i(TAG, "Error : device not found");
                        try {
                            Thread.sleep(2000,0);
                        } catch(Throwable t){
                        }
                        print_retry_counter.incrementAndGet();
                        continue;
                    }
                } catch (Exception e) {
                    Log.i(TAG, "Error : " + e.getMessage());
                    try {
                        Thread.sleep(1000,0);
                    } catch(Throwable t){
                    }
                    print_retry_counter.incrementAndGet();
                    continue;
                }
            }
            return 1;
        }

        private BluetoothDevice findPairedPrinter() {
            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    if (printerName.equalsIgnoreCase(device.getName())) {
                        return device;
                    }
                }
            }
            return null;
        }

        private boolean printMessage() throws IOException {
            Log.i(TAG, "Connecting");
            BluetoothDevice device = findPairedPrinter();
            if (device != null) {
                BluetoothSocket mSocket = device.createRfcommSocketToServiceRecord(
                        UUID.fromString(SPP_UUID)
                );
                mSocket.connect();
                OutputStream mOutputStream = mSocket.getOutputStream();
                Log.i(TAG, "Printing");
                mOutputStream.write(message.getBytes("GBK"));
                mOutputStream.flush();
                Log.i(TAG, "Closing socket");
                mSocket.close();
                return true;
            }
            return false;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(Integer result) {
            disconnect();
        }
    }
}
