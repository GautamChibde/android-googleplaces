package com.gts.infra.print;

public class BluetoothPrinterException extends RuntimeException {

    public BluetoothPrinterException(String s) {
        super(s);
    }
}
