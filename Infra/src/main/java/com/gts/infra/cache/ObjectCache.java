package com.gts.infra.cache;

import android.util.LruCache;

public class ObjectCache {
    private static final LruCache<Object, Object> cache = new LruCache<>(1000);

    public static Object get(Object key) {
        return cache.get(key);
    }

    public static synchronized void put(Object key, Object value) {
        cache.put(key, value);
    }

    public static int size() {
        return cache.size();
    }

    public static void clear() {
        cache.evictAll();
    }
}
