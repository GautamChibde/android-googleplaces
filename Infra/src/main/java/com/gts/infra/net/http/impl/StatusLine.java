package com.gts.infra.net.http.impl;

public class StatusLine {

    private int statusCode;
    private String reasonPhrase;

    public StatusLine(int responseCode, String responseMessage) {
        this.statusCode = responseCode;
        this.reasonPhrase = responseMessage;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public void setReasonPhrase(String reasonPhrase) {
        this.reasonPhrase = reasonPhrase;
    }

    public int getStatusCode(){
        return statusCode;
    }

    public String getReasonPhrase(){
        return reasonPhrase;
    }
}
