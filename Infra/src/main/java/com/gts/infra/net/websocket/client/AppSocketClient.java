package com.gts.infra.net.websocket.client;

import android.util.Log;


import com.gts.infra.net.websocket.handshake.ServerHandshake;

import java.net.URI;

import de.greenrobot.event.EventBus;

public class AppSocketClient extends WebSocketClient {

    private final EventBus eventBus = EventBus.getDefault();
    private final String TAG = AppSocketClient.class.getName();

    public AppSocketClient(URI serverURI) {
        super(serverURI);
    }

    @Override
    public void onOpen(ServerHandshake data) {
        eventBus.register(this);
        Log.i(TAG, "server handshake data : " + data.getHttpStatus() + "; " + data.getHttpStatusMessage());
        eventBus.post(new SocketOpenedEvent(data));
    }

    @Override
    public void onMessage(String message) {
        Log.i(TAG, "message : " + message);
        eventBus.post(new DataReceivedEvent(message));
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        Log.i(TAG, "code : " + code + "; reason : " + reason + "; remote: " + remote);
        eventBus.post(new SocketClosedEvent(code, reason, remote));
        if (eventBus.isRegistered(this)) {
            eventBus.unregister(this);
        }
    }

    @Override
    public void onError(Exception ex) {
    }

    @SuppressWarnings("unused")
    public void onEvent(DataSendEvent event) {
        send(event.getData());
    }

    public static class DataSendEvent {
        private final String data;

        public DataSendEvent(String data) {
            this.data = data;
        }

        public String getData() {
            return data;
        }
    }

    public static class DataReceivedEvent {
        private final String data;

        public DataReceivedEvent(String data) {
            this.data = data;
        }

        public String getData() {
            return data;
        }
    }

    public static class SocketOpenedEvent {
        private final ServerHandshake data;

        public SocketOpenedEvent(ServerHandshake data) {
            this.data = data;
        }

        public ServerHandshake getData() {
            return data;
        }
    }

    public static class SocketClosedEvent {
        private final int code;
        private final String reason;
        private final boolean remote;

        public SocketClosedEvent(int code, String reason, boolean remote) {
            this.code = code;
            this.reason = reason;
            this.remote = remote;
        }

        public int getCode() {
            return code;
        }

        public String getReason() {
            return reason;
        }

        public boolean isRemote() {
            return remote;
        }
    }


}
