package com.gts.infra.net.http.api.request;

import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.gts.infra.net.http.api.error.ParseError;
import com.gts.infra.net.http.api.response.Response;
import com.gts.infra.net.http.cache.Cache;
import com.gts.infra.net.http.impl.HttpHeaderParser;
import com.gts.infra.net.http.impl.NetworkResponse;
import com.gts.util.AnnotationExclusionStrategy;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;

public class GsonRequest<R, T> extends JsonRequest<T> {

    protected final Gson gson;
    protected final Type type;
    protected String mRequestBody;

    public GsonRequest(int method, String url, R requestBody,
                       Response.Listener<T> listener,
                       Response.ErrorListener errorListener, Type type) {
        super(method, url, null, listener, errorListener);
        this.type = type;

        gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .setExclusionStrategies(new AnnotationExclusionStrategy())
                .setDateFormat("yyyy-MM-dd")
                .create();
        if (requestBody != null) {
            mRequestBody = gson.toJson(requestBody);
            Log.d("GsonReqmRequestBody " + type.toString(), mRequestBody);
        }
    }

    @Override
    public Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return getResponse(json, HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    @SuppressWarnings("unchecked")
    protected Response<T> getResponse(String responseString, Cache.Entry cacheHeaders)
            throws UnsupportedEncodingException {
        return (Response<T>) Response.success(gson.fromJson(responseString, type), cacheHeaders);
    }

    @Override
    public byte[] getBody() {
        try {
            return mRequestBody == null ? null : mRequestBody.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException uee) {
            Log.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                    mRequestBody, uee);
            return null;
        }
    }
}
