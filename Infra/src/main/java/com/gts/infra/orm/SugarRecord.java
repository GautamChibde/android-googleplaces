package com.gts.infra.orm;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;
import android.text.TextUtils;
import android.util.Log;

import com.gts.infra.orm.dsl.Table;
import com.gts.infra.orm.util.NamingHelper;
import com.gts.infra.orm.util.QueryBuilder;
import com.gts.infra.orm.util.ReflectionUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.gts.infra.orm.SugarContext.getSugarContext;

@SuppressWarnings({"unused", "TryFinallyCanBeTryWithResources"})
public class SugarRecord {

    private static final String TAG = SugarRecord.class.getSimpleName();

    private static SQLiteDatabase getDb() {
        return getSugarContext().getSugarDb().getDB();
    }

    public static <T> int deleteAll(Class<T> type) {
        return deleteAll(type, null);
    }

    public static <T> int deleteAll(Class<T> type, String whereClause, String... whereArgs) {
        return getDb().delete(NamingHelper.toSQLName(type), whereClause, whereArgs);
    }

    public static <T> List<T> listAll(Class<T> type) {
        return find(type, null, null, null, null, null);
    }

    public static <T> List<T> listAll(Class<T> type, String orderBy) {
        return find(type, null, null, null, orderBy, null);
    }

    public static <T> T findById(Class<T> type, Long id) {
        List<T> list = find(type, "id=?", new String[]{String.valueOf(id)}, null, null, "1");
        if (list.isEmpty()) return null;
        return list.get(0);
    }

    public static <T> List<T> findById(Class<T> type, String[] ids) {
        String whereClause = "id IN (" + QueryBuilder.generatePlaceholders(ids.length) + ")";
        return find(type, whereClause, ids);
    }

    public static <T> List<T> find(Class<T> type, String whereClause, String... whereArgs) {
        return find(type, whereClause, whereArgs, null, null, null);
    }

    public static <T> List<T> findWithQuery(Class<T> type, String query, String... arguments) {
        T entity;
        List<T> toRet = new ArrayList<>();
        Cursor c = getDb().rawQuery(query, arguments);
        try {
            while (c.moveToNext()) {
                entity = type.getDeclaredConstructor().newInstance();
                inflate(c, entity, getSugarContext().getEntitiesMap());
                toRet.add(entity);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        } finally {
            c.close();
        }
        return toRet;
    }

    public static void executeQuery(String query, String... arguments) {
        getDb().execSQL(query, arguments);
    }

    public static <T> List<T> find(Class<T> type, String whereClause, String[] whereArgs,
                                   String groupBy, String orderBy, String limit) {
        T entity;
        List<T> toRet = new ArrayList<>();
        Cursor c = getDb().query(
                NamingHelper.toSQLName(type), null, whereClause,
                whereArgs, groupBy, null, orderBy, limit
        );
        try {
            while (c.moveToNext()) {
                entity = type.getDeclaredConstructor().newInstance();
                inflate(c, entity, getSugarContext().getEntitiesMap());
                toRet.add(entity);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        } finally {
            c.close();
        }
        return toRet;
    }


    public static long count(Class<?> type) {
        return count(type, null, null, null, null, null);
    }

    public static long count(Class<?> type, String whereClause, String[] whereArgs) {
        return count(type, whereClause, whereArgs, null, null, null);
    }

    public static long count(Class<?> type, String whereClause,
                             String[] whereArgs, String groupBy,
                             String orderBy, String limit) {
        long toRet = -1;
        String filter = (!TextUtils.isEmpty(whereClause)) ? " where " + whereClause : "";
        SQLiteStatement sqliteStatement;
        try {
            sqliteStatement = getDb().compileStatement(
                    "SELECT count(*) FROM " + NamingHelper.toSQLName(type) + filter
            );
        } catch (SQLiteException e) {
            e.printStackTrace();
            return toRet;
        }
        if (whereArgs != null) {
            for (int i = whereArgs.length; i != 0; i--) {
                sqliteStatement.bindString(i, whereArgs[i - 1]);
            }
        }
        try {
            toRet = sqliteStatement.simpleQueryForLong();
        } finally {
            sqliteStatement.close();
        }
        return toRet;
    }

    public static long save(Object object) {
        return save(getDb(), object);
    }

    static long save(SQLiteDatabase db, Object object) {
        Map<Object, Long> entitiesMap = getSugarContext().getEntitiesMap();
        List<Field> columns = ReflectionUtil.getTableFields(object.getClass());
        ContentValues values = new ContentValues(columns.size());
        Field idField = null;
        for (Field column : columns) {
            ReflectionUtil.addFieldValueToColumn(values, column, object, entitiesMap);
            if (column.getName().equals("id")) {
                idField = column;
            }
        }
        boolean isSugarEntity = isSugarEntity(object.getClass());
        if (isSugarEntity && entitiesMap.containsKey(object)) {
            values.put("id", entitiesMap.get(object));
        }
        Log.d("SugarRecord", values.toString());
        long id = db.insertWithOnConflict(NamingHelper.toSQLName(
                object.getClass()), null, values,
                SQLiteDatabase.CONFLICT_REPLACE
        );
        if (object.getClass().isAnnotationPresent(Table.class)) {
            if (idField != null) {
                idField.setAccessible(true);
                try {
                    idField.set(object, id);
                } catch (IllegalAccessException e) {
                    Log.e(TAG, e.getMessage());
                }
            } else {
                entitiesMap.put(object, id);
            }
        }
        Log.i(TAG, object.getClass().getSimpleName() + " saved : " + id);
        return id;
    }

    public static boolean isSugarEntity(Class<?> objectClass) {
        return objectClass.isAnnotationPresent(Table.class)
                || SugarRecord.class.isAssignableFrom(objectClass);
    }

    private static void inflate(Cursor cursor, Object object, Map<Object, Long> entitiesMap) {
        List<Field> columns = ReflectionUtil.getTableFields(object.getClass());
        if (!entitiesMap.containsKey(object)) {
            entitiesMap.put(object, cursor.getLong(cursor.getColumnIndex(("ID"))));
        }
        for (Field field : columns) {
            field.setAccessible(true);
            Class<?> fieldType = field.getType();
            if (isSugarEntity(fieldType)) {
                try {
                    long id = cursor.getLong(cursor.getColumnIndex(
                            NamingHelper.toSQLName(field))
                    );
                    field.set(object, (id > 0) ? findById(fieldType, id) : null);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            } else {
                ReflectionUtil.setFieldValueFromCursor(cursor, field, object);
            }
        }
    }

    public static boolean delete(Object object) {
        Class<?> type = object.getClass();
        if (type.isAnnotationPresent(Table.class)) {
            try {
                Field field = type.getDeclaredField("id");
                field.setAccessible(true);
                Long id = (Long) field.get(object);
                if (id != null && id > 0L) {
                    boolean deleted = getDb().delete(
                            NamingHelper.toSQLName(type), "Id=?",
                            new String[]{id.toString()}) == 1;
                    Log.i(TAG, type.getSimpleName() + " deleted : " + id);
                    return deleted;
                } else {
                    Log.i(TAG, "Cannot delete object: " +
                            object.getClass().getSimpleName() + " - object has not been saved");
                    return false;
                }
            } catch (NoSuchFieldException e) {
                Log.i(TAG, "Cannot delete object: "
                        + object.getClass().getSimpleName() + " - annotated object has no id");
                return false;
            } catch (IllegalAccessException e) {
                Log.i(TAG, "Cannot delete object: "
                        + object.getClass().getSimpleName() + " - can't access id");
                return false;
            }
        } else {
            Log.i(TAG, "Cannot delete object: "
                    + object.getClass().getSimpleName() + " - not persisted");
            return false;
        }
    }

}
