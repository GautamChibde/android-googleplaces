package com.gts.infra.orm.repo;

import com.gts.infra.orm.util.NamingHelper;

import java.util.List;

public class TreeRepo extends BaseRepo2 {

    public static <T> List<T> searchByName(Class<T> clazz, String query) {
        return search(clazz, "name like ? and state = 'active'", "%" + query + "%");
    }

    public static <T> T getRoot(Class<T> clazz) {
        List<T> found = searchWithQuery(
                clazz,
                getQueryRoot(clazz)
        );
        if (found.size() == 1) {
            return found.get(0);
        }
        return null;
    }

    public static <T> List<T> getNonRoot(Class<T> clazz) {
        return searchWithQuery(
                clazz,
                getQueryNonRoot(clazz)
        );
    }

    public static <T> List<T> getLeaf(Class<T> clazz) {
        return searchWithQuery(
                clazz,
                getQueryGetLeaf(clazz)
        );
    }

    private static String getQueryGetLeaf(Class clazz) {
        return "select p.* from " + NamingHelper.toSQLName(clazz) + " as p " +
                "where not exists " +
                "(select * from " + NamingHelper.toSQLName(clazz) + " as c " +
                "where c.path like (p.path || '.%')) and state = 'active'";
    }

    private static String getQueryRoot(Class clazz) {
        return "select * from " + NamingHelper.toSQLName(clazz) + " " +
                "where path = " +
                "(select min(path) " +
                " from " + NamingHelper.toSQLName(clazz) + ")  and state = 'active'";
    }

    private static String getQueryNonRoot(Class clazz) {
        return "select * from " + NamingHelper.toSQLName(clazz) + " " +
                "where path <> " +
                "(select min(path) " +
                " from " + NamingHelper.toSQLName(clazz) + ") and state = 'active'";
    }

}
