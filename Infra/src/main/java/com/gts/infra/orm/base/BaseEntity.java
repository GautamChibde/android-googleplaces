package com.gts.infra.orm.base;

import com.gts.infra.orm.dsl.Column;
import com.gts.infra.orm.dsl.Ignore;

import java.util.Date;

@SuppressWarnings("unused")
public abstract class BaseEntity extends BaseValueNamed {

    @Column(name = "updated_stamp")
    protected Date updatedStamp;

    @Ignore
    protected Long rol;

    @Override
    public boolean isValid() {
        return true;
    }

    public Date getUpdatedStamp() {
        return updatedStamp;
    }

    public void setUpdatedStamp(Date updatedStamp) {
        this.updatedStamp = updatedStamp;
    }

    public Long getRol() {
        return rol;
    }

    public void setRol(Long rol) {
        this.rol = rol;
    }

}