package com.gts.infra.orm.doc;

import com.gts.infra.orm.record.Record;

import java.lang.reflect.Type;

public class RecordMeta {

    public static String REQUEST_CODE = "REQUEST_CODE";
    public static String DOC = "DOC";
    public static String TYPE = "TYPE";
    public static String TYPE_LIST = "TYPE_LIST";
    public static String RESOURCE_URI = "RESOURCE_URI";

    private final int requestId;
    private String resourceUri;
    private Class<? extends Record> clazz;
    private Type typeObject;
    private Type typeList;
    private boolean syncDone;
    private String doc;

    public RecordMeta(int requestId) {
        this.requestId = requestId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecordMeta that = (RecordMeta) o;

        return requestId == that.requestId;

    }

    @Override
    public int hashCode() {
        return requestId;
    }

    public int getRequestId() {
        return requestId;
    }

    public String getResourceUri() {
        return resourceUri;
    }

    public void setResourceUri(String resourceUri) {
        this.resourceUri = resourceUri;
    }

    public Class<? extends Record> getClazz() {
        return clazz;
    }

    public void setClazz(Class<? extends Record> clazz) {
        this.clazz = clazz;
    }

    public Type getTypeObject() {
        return typeObject;
    }

    public void setTypeObject(Type typeObject) {
        this.typeObject = typeObject;
    }

    public Type getTypeList() {
        return typeList;
    }

    public void setTypeList(Type typeList) {
        this.typeList = typeList;
    }

    public boolean isSyncDone() {
        return syncDone;
    }

    public void setSyncDone(boolean syncDone) {
        this.syncDone = syncDone;
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }
}
