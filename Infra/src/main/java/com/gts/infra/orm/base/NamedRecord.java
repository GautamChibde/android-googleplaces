package com.gts.infra.orm.base;

import com.gts.infra.orm.record.Record;

public interface NamedRecord extends Record {

    String getName();
}
