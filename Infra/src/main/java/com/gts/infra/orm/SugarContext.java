package com.gts.infra.orm;

import android.content.Context;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class SugarContext {

    private static SugarContext instance = null;
    private SugarDb sugarDb;
    private ConcurrentMap<Object, Long> entitiesMap;

    public static final String DROP_EXISTING = "DROP_EXISTING";

    private SugarContext(Context context, Map<String, Object> config) {
        this.sugarDb = new SugarDb(context, config);
        this.entitiesMap = new ConcurrentHashMap<>(16, 0.75f, 4);
    }

    public static SugarContext getSugarContext() {
        if (instance == null) {
            throw new NullPointerException("SugarContext has not been initialized properly. ");
        }
        return instance;
    }

    public static void init(Context context, Map<String, Object> config) {
        instance = new SugarContext(context, config);
    }

    public static void terminate() {
        if (instance == null) {
            return;
        }
        instance.doTerminate();
    }

    private void doTerminate() {
        if (this.sugarDb != null) {
            this.sugarDb.getDB().close();
        }
    }

    protected SugarDb getSugarDb() {
        return sugarDb;
    }

    ConcurrentMap<Object, Long> getEntitiesMap() {
        return entitiesMap;
    }
}
