package com.gts.infra.orm.repo;

import com.gts.infra.App;
import com.gts.infra.data.SearchRequest;
import com.gts.infra.net.http.Resource;
import com.gts.infra.orm.SugarRecord;
import com.gts.infra.orm.base.BaseEntity;
import com.gts.infra.orm.util.NamingHelper;

import java.lang.reflect.Type;
import java.util.List;

public class BaseRepo<T> {

    public void save(T entity) {
        SugarRecord.save(entity);
    }

    public List<T> all(Class<T> clazz) {
        return SugarRecord.listAll(clazz);
    }

    public List<T> active(Class<T> clazz) {
        return SugarRecord.find(clazz, "state = 'active'");
    }

    public T findById(Class<T> clazz, Long id) {
        return SugarRecord.findById(clazz, id);
    }

    public List<T> search(Class<T> clazz, String selection, String... args) {
        return SugarRecord.find(clazz, selection, args);
    }

    public List<T> searchWithQuery(Class<T> clazz, String query, String... args) {
        return SugarRecord.findWithQuery(clazz, query, args);
    }

    public void delete(T entity) {
        SugarRecord.delete(entity);
    }

    public <E extends BaseEntity> void setInActive(E entity) {
        entity.setState("archived");
        SugarRecord.save(entity);
    }

    public T getLastUpdated(Class<T> clazz) {
        List<T> lastUpdatedList = SugarRecord.findWithQuery(clazz,
                "select * from " + NamingHelper.toSQLName(clazz) + " order by updated_stamp desc limit 1");
        if (lastUpdatedList.size() > 0) {
            return lastUpdatedList.get(0);
        } else {
            try {
                return clazz.newInstance();
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void fetch(App app, SearchRequest request, Type typeToken, int requestId) {
        Resource.postAsync(
                app,
                "/api/search",
                requestId,
                typeToken,
                request,
                app.getClientToken()
        );
    }


}
