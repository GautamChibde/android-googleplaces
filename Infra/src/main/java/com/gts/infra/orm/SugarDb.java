package com.gts.infra.orm;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.gts.infra.orm.util.ManifestHelper;
import com.gts.infra.orm.util.SugarCursorFactory;

import java.util.Map;

import static com.gts.infra.orm.util.ManifestHelper.getDatabaseVersion;
import static com.gts.infra.orm.util.ManifestHelper.getDebugEnabled;

public class SugarDb extends SQLiteOpenHelper {

    private final SchemaGenerator schemaGenerator;
    private SQLiteDatabase sqLiteDatabase;
    private Map<String, Object> config;

    public SugarDb(Context context, Map<String, Object> config) {
        super(context, ManifestHelper.getDatabaseName(context),
                new SugarCursorFactory(getDebugEnabled(context)),
                getDatabaseVersion(context));
        this.config = config;
        schemaGenerator = new SchemaGenerator(context);
        if (shouldDropExisting()) {
            sqLiteDatabase = getWritableDatabase();
            drop(sqLiteDatabase);
            schemaGenerator.createDatabase(sqLiteDatabase);
        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        if (shouldDropExisting()) {
            drop(sqLiteDatabase);
        }
        schemaGenerator.createDatabase(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        schemaGenerator.doUpgrade(sqLiteDatabase, oldVersion, newVersion);
    }

    public void drop(SQLiteDatabase sqLiteDatabase) {
        schemaGenerator.deleteTables(sqLiteDatabase);
    }

    public synchronized SQLiteDatabase getDB() {
        if (this.sqLiteDatabase == null) {
            this.sqLiteDatabase = getWritableDatabase();
        }

        return this.sqLiteDatabase;
    }

    private boolean shouldDropExisting() {
        if (null != config) {
            if (null != config.get(SugarContext.DROP_EXISTING)
                    && config.get(SugarContext.DROP_EXISTING) instanceof Boolean
                    && (Boolean) config.get(SugarContext.DROP_EXISTING)) {
                return true;
            }
        }
        return false;
    }
}
