package com.gts.infra.event;

import android.os.Bundle;
import android.util.Log;

import java.io.Serializable;

public class LoadViewEvent implements Serializable {
    private final Object component;
    private final Bundle data;
    private final boolean addToBackStack;

    public LoadViewEvent(Object component) {
        this(component, new Bundle(), false);
    }

    public LoadViewEvent(Object component, Bundle data) {
        this(component, data, false);
    }

    public LoadViewEvent(Object component, Bundle data, boolean addToBackStack) {
        this.component = component;
        this.data = data;
        this.addToBackStack = addToBackStack;
    }

    public Object getComponent() {
        return component;
    }

    public Bundle getData() {
        return data;
    }

    public boolean isAddToBackStack() {
        return addToBackStack;
    }
}
