package com.gts.infra;


import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gts.infra.auth.AuthManager;
import com.gts.infra.event.LoadViewEvent;
import com.gts.infra.net.NetworkManager;
import com.gts.infra.net.http.api.request.RequestQueue;
import com.gts.infra.net.http.cache.BitmapLruCache;
import com.gts.infra.net.http.cache.Cache;
import com.gts.infra.net.http.cache.NoCache;
import com.gts.infra.net.http.impl.ImageLoader;
import com.gts.infra.orm.SugarContext;
import com.gts.infra.orm.doc.Store;
import com.gts.util.Constant;
import com.gts.util.PlatformUtil;

import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;

@SuppressWarnings("unused")
public class App extends Application {

    private static final String DEFAULT_CACHE_DIR = "volley";
    private static final String TAG = App.class.getSimpleName();
    protected final EventBus bus = EventBus.getDefault();
    protected RequestQueue queue;
    private SharedPreferences sharedPref;
    private ImageLoader imageLoader;
    private Gson gson;

    private AuthManager authManager;
    private NetworkManager networkManager;
    private Store store;

    @Override
    public void onCreate() {
        super.onCreate();
        if (null != getSharedPrefKey()) {
            sharedPref = getSharedPreferences(getSharedPrefKey(), MODE_PRIVATE);
        } else {
            throw new RuntimeException("Please specify Shared pref key");
        }
        authManager = new AuthManager(this);
        networkManager = new NetworkManager(this);
        store = new Store(this);
        registerListener(store, this);
        setAppFonts();
    }

    protected void startNewActivity(Class clazz, Bundle data) {
        Intent intent = new Intent(this, clazz);
        intent.putExtras(data == null ? new Bundle() : data);
        startActivity(intent);
        //if (data.getBoolean(Constant.FOR_RESULT)) {
        //  startActivityForResult(intent, data.getInt(Constant.REQUEST_CODE));
        //} else {
        //
        //}
    }

    public void onEventMainThread(LoadViewEvent event) {
        if (event.getComponent() instanceof Class
                && Activity.class.isAssignableFrom((Class) event.getComponent())) {
            startNewActivity((Class) event.getComponent(), event.getData());
        }
    }

    @Override
    public void onTerminate() {
        try {
            SugarContext.terminate();
            queue.stop();
        } catch (Throwable t) {
            VolleyLog.d(TAG, "Error during app terminate: " + t.getMessage());
        }
        super.onTerminate();
    }


    public void init() {
        init(false);
    }

    public void init(boolean recreateDatabase) {
        initLocalDb(recreateDatabase);
        initHttpQueue();
    }

    // ORM
    protected void initLocalDb(boolean recreateDatabase) {
        Map<String, Object> config = new HashMap<>();
        config.put(SugarContext.DROP_EXISTING, recreateDatabase);
        SugarContext.init(getApplicationContext(), config);
    }

    // Network
    public String getBaseUri() {
        return "";
    }

    public String getWebSocketUri() {
        return "";
    }

    public RequestQueue getQueue() {
        return queue;
    }

    protected void initHttpQueue() {
        queue = RequestQueue
                .build(getCache())
                .start();
    }

    // Event Bus

    public EventBus getBus() {
        return bus;
    }

    public void unRegisterListener(Object... listeners) {
        for (Object listener : listeners) {
            if (bus.isRegistered(listener)) {
                bus.unregister(listener);
            }
        }
    }

    public void registerListener(Object... listeners) {
        for (Object listener : listeners) {
            if (!bus.isRegistered(listener)) {
                bus.register(listener);
            }
        }
    }

    protected void registerListenerSticky(Object... listeners) {
        for (Object listener : listeners) {
            if (!bus.isRegistered(listener)) {
                bus.registerSticky(listener);
            }
        }
    }

    // Fonts
    protected String getRegularFontAssetName() {
        return null;
    }

    protected String getThinFontAssetName() {
        return null;
    }

    private void setAppFonts() {
        if (getThinFontAssetName() != null) {
            PlatformUtil.setDefaultFont(this, "MONOSPACE", getThinFontAssetName());
        }
        if (getRegularFontAssetName() != null) {
            PlatformUtil.setDefaultFont(this, "SERIF", getRegularFontAssetName());
        }
    }

    // Shared Preferences
    public String getSharedPrefKey() {
        return "";
    }

    public void saveToPref(String key, String value) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void saveToPref(Map<String, Object> values) {
        SharedPreferences.Editor editor = sharedPref.edit();
        for (String key : values.keySet()) {
            if (values.get(key) instanceof String) {
                editor.putString(key, (String) values.get(key));
            } else if (values.get(key) instanceof Long) {
                editor.putLong(key, (Long) values.get(key));
            }
        }
        editor.apply();
    }

    public String getFromPref(String key) {
        return sharedPref.getString(key, null);
    }

    public SharedPreferences getSharedPref() {
        return sharedPref;
    }

    // Auth
    public String getAccountType() {
        return "";
    }

    public String[] getAppCredentials() {
        return new String[]{
                "",
                ""
        };
    }

    public String getUserToken() {
        return getFromPref(Constant.KEY_USER_TOKEN);
    }

    public String getClientToken() {
        return getFromPref(Constant.KEY_CLIENT_TOKEN);
    }

    public String getSecretKey() {
        return null;
    }

    public String getStripeId() {
        return null;
    }

    // Cache
    @NonNull
    private Cache getCache() {
        return new NoCache();
    }

    // Metadata
    public String getMetaDataValue(String key) {
        try {
            ApplicationInfo ai = getPackageManager()
                    .getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
            return ai.metaData.getString(key);
        } catch (Exception e) {
            //logError(App.class.getName(), "getMetaDataValue", "Error : " + e.getMessage());
            return null;
        }
    }

    // Service

    public AuthManager getAuthManager() {
        return authManager;
    }

    public NetworkManager getNetworkManager() {
        return networkManager;
    }

    public Store getStore() {
        return store;
    }

    // Assets

    public synchronized ImageLoader getImageLoader() {
        if (imageLoader == null) {
            imageLoader = new ImageLoader(
                    getQueue(),
                    new BitmapLruCache()
            );
        }
        return imageLoader;
    }

    public void setImageLoader(ImageLoader imageLoader) {
        this.imageLoader = imageLoader;
    }

    public String getAssetBaseUri() {
        return "";
    }

    public Gson getGson() {
        if (gson == null) {
            gson = new GsonBuilder()
                    .setFieldNamingPolicy(
                            FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES
                    )
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                    .create();
        }
        return gson;
    }

}
