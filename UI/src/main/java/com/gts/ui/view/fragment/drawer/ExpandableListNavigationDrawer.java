package com.gts.ui.view.fragment.drawer;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.gts.ui.view.ViewFactory;
import com.gts.ui.view.adapter.elist.NavigationDrawerAdapter;
import com.gts.ui.view.model.Link;

public abstract class ExpandableListNavigationDrawer extends NavigationDrawer {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = ViewFactory.getDrawerExpandableListView(
                getActivity(),
                getAdapter(),
                new ExpandableListView.OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v,
                                                int groupPosition, int childPosition,
                                                long id) {
                        drawerItemClicked(
                                (Link) parent.getExpandableListAdapter().getChild(
                                        groupPosition, childPosition
                                )
                        );
                        return true;
                    }
                });
        root.setBackgroundColor(Color.WHITE);
        return root;
    }

    public abstract NavigationDrawerAdapter getAdapter();

}

