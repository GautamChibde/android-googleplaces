package com.gts.ui.view.component.control;

import android.content.Context;
import android.support.annotation.CallSuper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.gts.infra.orm.base.Model;
import com.gts.ui.R;
import com.gts.ui.view.component.control.listener.ActionListener;
import com.gts.ui.view.component.control.view.ReadOnlyText;
import com.gts.util.PlatformUtil;

import java.util.List;

import de.greenrobot.event.EventBus;

public abstract class BaseControl<T extends Model> extends LinearLayout {

    protected EventBus bus = EventBus.getDefault();

    protected ReadOnlyText<T> readView = new ReadOnlyText<T>(getContext()) {
        @Override
        protected void onClick() {
            onEditClick();
        }
    };
    protected TextView labelView = new TextView(getContext());
    protected ActionListener<T> listener;
    protected LinearLayout editContainer = new LinearLayout(getContext());
    protected ViewSwitcher switcher = new ViewSwitcher(getContext());

    public BaseControl(Context context) {
        super(context);
    }

    public BaseControl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        setOrientation(VERTICAL);
        LinearLayout.LayoutParams editViewParams =
                new LinearLayout.LayoutParams(
                        0, ViewGroup.LayoutParams.MATCH_PARENT, 1f
                );
        LinearLayout.LayoutParams btnCloseParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        );
        ImageButton btnClose = new ImageButton(getContext());
        btnClose.setImageResource(R.drawable.ic_close_black_24dp);
        btnClose.setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        switcher.showPrevious();
                        PlatformUtil.hideKeyboard(getContext(), v);
                        if (listener != null) {
                            listener.onClose();
                        }
                    }
                }
        );
        editContainer.addView(btnClose, btnCloseParams);
        editContainer.addView(getEditView(), editViewParams);
        setPadding(10, 0, 10, 0);
        LayoutParams defaultParams = new LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT
        );
        switcher.addView(readView, 0, defaultParams);
        switcher.addView(editContainer, 1, defaultParams);
        addView(labelView);
        addView(switcher);
        Log.d("BaseControl", "switcher" + switcher);
    }

    protected void onEditClick() {
        switcher.showNext();
    }

    protected abstract View getEditView();

    public void setModel(T model) {
        readView.setModel(model);
    }

    public T getModel() {
        return readView.getModel();
    }

    public void setListener(ActionListener<T> listener) {
        this.listener = listener;
    }

    public void setLabel(String label) {
        labelView.setText(label);
    }

    protected void validationFailed() {
    }

    protected class InputActionListener extends ActionListener.SimpleListener<T> {

        @Override
        public void onSubmit(T newValue) {
            if (listener.isValid(newValue)) {
                switcher.showPrevious();
                setModel(newValue);
                listener.onSubmit(newValue);
            } else {
                validationFailed();
            }
        }

        @Override
        public void onClose() {
            switcher.showPrevious();
            if (listener != null) {
                listener.onClose();
            }
        }

        @Override
        public List<T> onSuggestiveTextInput(String suggestiveInput) {
            return listener.onSuggestiveTextInput(suggestiveInput);
        }
    }
}
