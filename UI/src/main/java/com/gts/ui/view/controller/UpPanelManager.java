package com.gts.ui.view.controller;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

public class UpPanelManager {
    private final SlidingUpPanelLayout panelLayout;

    public enum STATE {
        EXPANDED,
        COLLAPSED,
        ANCHORED,
        HIDDEN,
        DRAGGING
    }

    public UpPanelManager(SlidingUpPanelLayout panelLayout) {
        this.panelLayout = panelLayout;
    }

    public void setState(SlidingUpPanelLayout.PanelState state) {
        if (!panelLayout.getPanelState().equals(state)) {
            panelLayout.setPanelState(state);
        }
    }

    public void setTouchEnabled(boolean touchEnabled) {
        panelLayout.setTouchEnabled(touchEnabled);
    }

    public SlidingUpPanelLayout getPanelLayout() {
        return panelLayout;
    }

    public void onEvent(TogglePanelEvent event) {
        setState(
                SlidingUpPanelLayout.PanelState.valueOf(
                        event.getState().name()
                )
        );
    }

    public boolean isState(SlidingUpPanelLayout.PanelState state) {
        return panelLayout.getPanelState() == state;
    }

    public static class TogglePanelEvent {
        private final STATE state;

        public TogglePanelEvent() {
            this.state = null;
        }

        public TogglePanelEvent(STATE state) {
            this.state = state;
        }

        public STATE getState() {
            return state;
        }
    }


}
