package com.gts.ui.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gts.ui.view.ViewFactory;
import com.gts.ui.view.adapter.recycler.AbstractRecyclerAdapter;
import com.gts.util.Constant;

public abstract class AbstractRecyclerFragment<T> extends AbstractFragment {

    protected AbstractRecyclerAdapter<T, ?> adapter;

    protected RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        recyclerView = ViewFactory.getVerticalRecycler(
                getActivity(),
                adapter = getAdapter()
        );
        getApp().registerListener(adapter);
        customizeView();
        return recyclerView;
    }

    protected void customizeView() {
    }

    @Override
    public void onDestroy() {
        getApp().unRegisterListener(adapter);
        super.onDestroy();
    }

    @SuppressWarnings("unused")
    public void onEvent(AbstractRecyclerAdapter.ItemSelectedEvent<T> event) {
        showChangeDialog(event.getEntity());
    }

    @SuppressWarnings("unused")
    public void onEvent(AbstractRecyclerAdapter.DeleteClickedEvent<T> event) {
        showConfirmDialog(event.getEntity());
    }

    protected void showChangeDialog(T entity) {
    }

    protected void showConfirmDialog(T entity) {
        Bundle data = new Bundle();
        data.putSerializable(Constant.MESSAGE, getMessage(entity));
        data.putSerializable(Constant.LISTENER, getActionListener(entity));
        showDialog(
                new ConfirmDialog(),
                data,
                ConfirmDialog.class.getName()
        );
    }

    protected String getMessage(T entity) {
        return "Confirm delete ?";
    }

    protected ConfirmDialog.ActionListener getActionListener(T entity) {
        return null;
    }


    protected abstract AbstractRecyclerAdapter<T, ?> getAdapter();

}
