package com.gts.ui.view.fragment.drawer;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.gts.ui.view.ViewFactory;
import com.gts.ui.view.adapter.list.ItemListAdapter;
import com.gts.ui.view.model.Link;

public abstract class ListNavigationDrawer extends NavigationDrawer {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = ViewFactory.getListView(
                getActivity(),
                getAdapter(),
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent,
                                            View view,
                                            int position,
                                            long id) {
                        drawerItemClicked(
                                (Link) parent.getItemAtPosition(position)
                        );
                    }
                });
        root.setBackgroundColor(Color.WHITE);
        return root;
    }

    protected abstract ItemListAdapter getAdapter();
}
