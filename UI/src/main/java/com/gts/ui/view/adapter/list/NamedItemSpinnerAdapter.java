package com.gts.ui.view.adapter.list;

import android.content.Context;

import com.gts.infra.orm.base.NamedRecord;

import java.util.List;

public class NamedItemSpinnerAdapter<T extends NamedRecord>
        extends AbstractSimpleListItemAdapter<T> {

    public NamedItemSpinnerAdapter(Context context, List<T> objects) {
        super(context, objects);
    }

    @Override
    protected String getText(T item) {
        return item.getName();
    }
}
