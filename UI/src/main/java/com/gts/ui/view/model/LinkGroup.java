package com.gts.ui.view.model;

import java.util.ArrayList;
import java.util.List;

public class LinkGroup {
    private final String name;
    private final List<Link> links;

    public LinkGroup(String name) {
        this.name = name;
        this.links = new ArrayList<>();
    }

    public LinkGroup(String name, List<Link> links) {
        this.name = name;
        this.links = links;
    }

    public String getName() {
        return name;
    }

    public List<Link> getLinks() {
        return links;
    }
}
