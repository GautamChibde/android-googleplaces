package com.gts.ui.view.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.gts.infra.App;
import com.gts.ui.R;
import com.gts.infra.event.LoadViewEvent;
import com.gts.ui.view.activity.AppActivity;
import com.gts.ui.view.controller.UpPanelManager;
import com.gts.util.PlatformUtil;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

@SuppressWarnings("unused")
public class AbstractDialogFragment extends DialogFragment {
    private CharSequence title;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getLayoutId() != 0) {
            View root = inflater.inflate(getLayoutId(), container, false);
            return root;
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected int getLayoutId() {
        return 0;
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            );
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        if (getDialogAnimation() != 0) {
            dialog.getWindow().getAttributes().windowAnimations = getDialogAnimation();
        }
//        WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
//        layoutParams.gravity = Gravity.TOP | Gravity.START;
//        layoutParams.x = 0;
//        layoutParams.y = 0;

        dialog.show();
        return dialog;
    }

    protected void setupToolbar(Toolbar toolbar, String title,
                                int backDrawable, int menu, int titleId,
                                Toolbar.OnMenuItemClickListener listener) {
//        toolbar.setNavigationIcon(
//                ContextCompat.getDrawable(
//                        getActivity(),
//                        backDrawable
//                )
//        );
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dismiss();
//            }
//        });
        toolbar.setOnMenuItemClickListener(listener);
        if (menu != 0) {
            toolbar.inflateMenu(menu);
        }
        if (titleId != 0) {
            TextView tvTitle = (TextView) toolbar.findViewById(titleId);
            tvTitle.setText(title);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    protected int getDialogAnimation() {
        return 0;
    }

    protected void showDialog(DialogFragment fragment, Bundle dataBundle, String tag) {
        fragment.setArguments(dataBundle);
        fragment.show(getActivity().getFragmentManager(), tag);
    }

    public void onRequestPermissionsResult(int requestId,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
    }

    protected boolean hasPermission(String permission) {
        return !TextUtils.isEmpty(permission) &&
                PackageManager.PERMISSION_GRANTED ==
                        ContextCompat.checkSelfPermission(getActivity(), permission);
    }
}
