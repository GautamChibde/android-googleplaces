package com.gts.ui.view.listener;

import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class GroupedTextWatcher implements TextWatcher {

    private final EditText etField;
    private final String separator;
    private final int totalBlocks;
    private final int blockSize;

    private String lastValue;

    public GroupedTextWatcher(EditText etField, String separator,
                              int totalBlocks, int blockSize) {
        this.etField = etField;
        this.separator = separator;
        this.totalBlocks = totalBlocks;
        this.blockSize = blockSize;
    }

    @Override
    public void afterTextChanged(Editable number) {
        if (isBlockOverflow(number.toString())) {
            setNewText(lastValue);
        } else {
            appendSeparator(number.toString());
            lastValue = etField.getText().toString();
        }
    }

    private boolean isBlockOverflow(String number) {
        String blocks[] = number.split(separator);
        for (String aBlock : blocks) {
            if (aBlock.length() > blockSize) {
                if (blocks.length != totalBlocks &&
                        aBlock.length() == blockSize + 1 &&
                        number.endsWith(aBlock)) {
                    setNewText(insertSeparator(number, aBlock));
                }
                return true;
            }
        }
        return false;
    }

    @NonNull
    private String insertSeparator(String number, String aBlock) {
        return number.substring(0, number.indexOf(aBlock) + blockSize) +
                separator +
                number.charAt(number.indexOf(aBlock) + blockSize);
    }

    private void appendSeparator(String number) {
        if (((number.length() + 1) % (blockSize + 1)) == 0) {
            if (number.split(separator).length < totalBlocks) {
                if (!(lastValue.endsWith(separator) && !number.endsWith(separator))) {
                    setNewText(number + separator);
                }
            }
        }
    }

    private void setNewText(String newValue) {
        etField.setText(newValue);
        etField.setSelection(etField.getText().length());
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }
}
