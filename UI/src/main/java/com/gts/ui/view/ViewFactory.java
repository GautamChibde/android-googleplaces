package com.gts.ui.view;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.gts.ui.view.adapter.elist.NavigationDrawerAdapter;
import com.gts.ui.view.adapter.list.AbstractListAdapter;
import com.gts.util.PlatformUtil;

@SuppressWarnings("unused")
public final class ViewFactory {

    public static RecyclerView getVerticalRecycler(Context context, RecyclerView.Adapter adapter) {
        RecyclerView recycler = new RecyclerView(context);
        recycler.setLayoutParams(
                new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
        recycler.setLayoutManager(new LinearLayoutManager(context));
        recycler.setAdapter(adapter);
        return recycler;
    }

    public static RecyclerView getRecycler(Context context, RecyclerView.Adapter adapter, int noOfCol) {
        RecyclerView recycler = new RecyclerView(context);
        recycler.setId(PlatformUtil.generateViewId());
        recycler.setLayoutParams(
                new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
        if (noOfCol == 1) {
            recycler.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recycler.setLayoutManager(new GridLayoutManager(context, noOfCol));
        }
        recycler.setAdapter(adapter);
        return recycler;
    }

    public static RecyclerView getTwoColumnRecycler(Context context, RecyclerView.Adapter adapter) {
        RecyclerView recycler = new RecyclerView(context);
        recycler.setId(PlatformUtil.generateViewId());
        recycler.setLayoutParams(
                new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
        recycler.setLayoutManager(new GridLayoutManager(context, 2));
        recycler.setAdapter(adapter);
        return recycler;
    }

    public static ListView getListView(Context context,
                                       AbstractListAdapter adapter,
                                       AdapterView.OnItemClickListener listener) {
        ListView listView = new ListView(context);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(listener);
        listView.setLayoutParams(
                new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
        return listView;
    }

    public static TextView getDrawerItemView(Context context, int textSize,
                                             int drawablePadding) {
        return getDrawerItemView(context, textSize, drawablePadding, Color.BLACK);
    }

    public static TextView getDrawerItemView(Context context, int textSize,
                                             int drawablePadding, int color) {
        TextView drawerItemView = new TextView(context);
        drawerItemView.setTextSize(textSize);
        drawerItemView.setCompoundDrawablePadding(drawablePadding);
        drawerItemView.setPadding(16, 12, 16, 12);
        drawerItemView.setTextColor(color);
        drawerItemView.setLayoutParams(
                new ListView.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
        return drawerItemView;
    }

    private ViewFactory() {
        throw new IllegalStateException("Factory class. Do not instantiate");
    }

    public static ExpandableListView getDrawerExpandableListView(
            Context context, NavigationDrawerAdapter adapter,
            ExpandableListView.OnChildClickListener onChildClickListener) {
        ExpandableListView expandableListView = new ExpandableListView(context);
        expandableListView.setLayoutParams(
                new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
        expandableListView.setAdapter(adapter);
        expandableListView.setOnChildClickListener(onChildClickListener);
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return true;
            }
        });
        for (int i = 0; i < expandableListView.getExpandableListAdapter().getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }
        expandableListView.setGroupIndicator(null);
        expandableListView.setChildIndicator(null);
        expandableListView.setChildDivider(null);
        expandableListView.setDivider(null);
        expandableListView.setDividerHeight(0);
        expandableListView.setGroupIndicator(null);
        return expandableListView;
    }

    public static LinearLayout getRootLinearLayout(Context context) {
        LinearLayout root = new LinearLayout(context);
        root.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        root.setOrientation(LinearLayout.VERTICAL);
        return root;
    }

    public static LinearLayout getHorizontalFull(Context context) {
        LinearLayout root = new LinearLayout(context);
        root.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        root.setOrientation(LinearLayout.HORIZONTAL);
        return root;
    }

    public static ViewPager getViewPager(Context context, FragmentStatePagerAdapter adapter) {
        ViewPager viewPager = new ViewPager(context);
        viewPager.setId(PlatformUtil.generateViewId());
        viewPager.setAdapter(adapter);
        return viewPager;
    }

    public static Snackbar getAuthErrorSnackbar(
            View view, String message,
            int textViewId,
            int backgroundColor,
            int textColor) {
        Snackbar snackbar = Snackbar.make(
                view,
                message,
                Snackbar.LENGTH_LONG
        );
        View sbView = snackbar.getView();
        sbView.setPadding(0, 0, 0, 0);
        TextView textView =
                (TextView) sbView.findViewById(
                        textViewId
                );
        if (textColor != 0) {
            textView.setTextColor(textColor);
        }
        if (backgroundColor != 0) {
            textView.setBackgroundColor(backgroundColor);
        }
        return snackbar;
    }
}
