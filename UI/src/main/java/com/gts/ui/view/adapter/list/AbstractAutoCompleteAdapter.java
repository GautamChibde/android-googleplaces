package com.gts.ui.view.adapter.list;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractAutoCompleteAdapter<T> extends AbstractListAdapter<T> {

    private final int backgroundColor;
    private final int textColor;
    private List<T> items = new ArrayList<>();

    public AbstractAutoCompleteAdapter(Context context) {
        super(context, new ArrayList<T>());
        this.backgroundColor = Color.WHITE;
        this.textColor = Color.BLACK;
    }

    public AbstractAutoCompleteAdapter(Context context, int backgroundColor, int textColor) {
        super(context, new ArrayList<T>());
        this.backgroundColor = backgroundColor == 0 ? Color.WHITE : backgroundColor;
        this.textColor = textColor == 0 ? Color.BLACK : textColor;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    @Override
    public T getItem(int position) {
        if (items.size() > position) {
            return items.get(position);
        } else {
            return null;
        }
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    protected View getView(int position, ViewGroup parent) {
        return inflater.inflate(
                android.R.layout.simple_list_item_1,
                parent,
                false);
    }

    @Override
    protected void initView(View view, int position) {
        TextView tv = (TextView) view;
        tv.setText(getLabel(getItem(position)));
        tv.setBackgroundColor(backgroundColor);
        tv.setTextColor(textColor);
    }

    protected abstract List<T> performFiltering(CharSequence constraint);

    protected abstract String getLabel(T aValue);

    private Filter mFilter = new Filter() {
        @SuppressWarnings("unchecked")
        @Override
        public String convertResultToString(Object resultValue) {
            return getLabel((T) resultValue);
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null) {
                ArrayList<T> suggestions = new ArrayList<>();
                for (T aValue : AbstractAutoCompleteAdapter.this.performFiltering(constraint)) {
                    if (getLabel(aValue) != null &&
                            getLabel(aValue).toLowerCase().contains(
                                    constraint.toString().toLowerCase())) {
                        suggestions.add(aValue);
                    }
                }
                results.values = suggestions;
                results.count = suggestions.size();
            }
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            items.clear();
            if (results != null && results.count > 0) {
                items.addAll((ArrayList<T>) results.values);
            }
            //else {
            //  addAll(master);
            //}
            notifyDataSetChanged();
        }
    };


}
