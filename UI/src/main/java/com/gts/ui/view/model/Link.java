package com.gts.ui.view.model;

import android.app.Fragment;

import java.io.Serializable;

public class Link implements Serializable {

    private final String name;
    private final int imageId;
    private final Class viewToShow;

    public Link() {
        this.name = "";
        this.imageId = 0;
        viewToShow = Fragment.class;
    }

    public Link(String name, int imageId) {
        this.name = name;
        this.imageId = imageId;
        viewToShow = Fragment.class;
    }

    public Link(String name, int imageId, Class viewToShow) {
        this.name = name;
        this.imageId = imageId;
        this.viewToShow = viewToShow;
    }

    @Override
    public String toString() {
        return "Link{" +
                "name='" + name + '\'' +
                ", imageId=" + imageId +
                ", viewToShow=" + viewToShow +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Link link = (Link) o;

        return name.equals(link.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public String getName() {
        return name;
    }

    public int getImageId() {
        return imageId;
    }

    public Class getViewToShow() {
        return viewToShow;
    }
}
