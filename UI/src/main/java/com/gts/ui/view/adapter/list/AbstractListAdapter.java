package com.gts.ui.view.adapter.list;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

public abstract class AbstractListAdapter<T> extends ArrayAdapter<T> {

    protected final LayoutInflater inflater;
    protected List<T> itemList = new ArrayList<>();

    public AbstractListAdapter(Context context, List<T> itemList) {
        super(context, 0, itemList);
        this.itemList = itemList;
        inflater = LayoutInflater.from(context);
    }

    public AbstractListAdapter(Context context) {
        this(context, new ArrayList<T>());
        new LoadTask().execute();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View toReturn;
        if (convertView == null) {
            toReturn = getView(position, parent);
        } else {
            toReturn = convertView;
        }
        initView(toReturn, position);
        return toReturn;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View toReturn;
        if (convertView == null) {
            toReturn = getView(position, parent);
        } else {
            toReturn = convertView;
        }
        initView(toReturn, position);
        return toReturn;
    }

    protected void post(Object event) {
        EventBus.getDefault().post(event);
    }

    protected abstract View getView(final int position, ViewGroup parent);

    protected abstract void initView(View view, int position);

    public void reload() {
        new LoadTask(true).execute();
    }

    public List<T> getData(long lastId) {
        return new ArrayList<>();
    }

    private class LoadTask extends AsyncTask<Void, Void, List<T>> {
        private final boolean clearExisting;

        public LoadTask() {
            this.clearExisting = false;
        }

        public LoadTask(boolean clearExisting) {
            this.clearExisting = clearExisting;
        }

        @Override
        protected List<T> doInBackground(Void... params) {
            return getData(-1);
        }

        @Override
        protected void onPostExecute(List<T> ts) {
            if (clearExisting) {
                clear();
                itemList.addAll(ts);
                notifyDataSetChanged();
            } else {
                if (ts != null && ts.size() > 0) {
                    itemList.addAll(ts);
                    notifyDataSetChanged();
                }
            }

        }
    }
}
