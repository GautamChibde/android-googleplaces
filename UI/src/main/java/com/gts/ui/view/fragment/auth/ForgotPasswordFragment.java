package com.gts.ui.view.fragment.auth;

import com.gts.ui.R;

public class ForgotPasswordFragment extends AbstractAuthFragment {

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_forgot_password;
    }

    @Override
    protected String getTitle() {
        return "Forgot password";
    }

    @Override
    protected boolean dataValid() {
        return false;
    }

    @Override
    protected void onSubmit() {
    }


}
