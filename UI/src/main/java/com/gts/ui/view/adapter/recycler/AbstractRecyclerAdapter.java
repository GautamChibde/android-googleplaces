package com.gts.ui.view.adapter.recycler;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.gts.infra.cache.ObjectCache;
import com.gts.infra.event.LoadViewEvent;
import com.gts.ui.view.model.Filter;
import com.gts.util.PlatformUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import de.greenrobot.event.EventBus;

@SuppressWarnings("unused")
public abstract class
AbstractRecyclerAdapter<T, VH extends ViewHolder<T>>
        extends RecyclerView.Adapter<VH> {

    protected LayoutInflater inflater;
    private List<T> itemList;
    private final Set<Filter<T>> cachedFilters;
    protected final List<T> originalList;
    protected final Context context;

    protected int pageCount = 10;

    @SuppressWarnings("unchecked")
    public AbstractRecyclerAdapter(Context context, List<T> itemList) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.itemList = itemList;
        if (!TextUtils.isEmpty(getCacheFilterKey())) {
            Set<Filter<T>> fromCache = (Set<Filter<T>>) ObjectCache.get(getCacheFilterKey());
            if (null == fromCache) {
                cachedFilters = new HashSet<>();
                ObjectCache.put(getCacheFilterKey(), cachedFilters);
            } else {
                cachedFilters = fromCache;
            }
            this.originalList = new ArrayList<>(itemList);
            applyFilters();
        } else {
            originalList = new ArrayList<>();
            cachedFilters = new HashSet<>();
        }
    }

    public Context getContext() {
        return context;
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        holder.initView(getItem(position));
    }

    public T getItem(int position) {
        return itemList.get(position);
    }

    protected void sort(Comparator<T> comparator) {
        Collections.sort(itemList, comparator);
        notifyDataSetChanged();
    }

    protected void applyFilters() {
        boolean changed = false;
        if (itemList != null && itemList.size() != originalList.size()) {
            itemList.clear();
            itemList.addAll(originalList);
            changed = true;
        }
        if (cachedFilters.size() > 0 && itemList.size() > 0) {
            Iterator<T> it = itemList.iterator();
            while (it.hasNext()) {
                T foo = it.next();
                for (Filter<T> filter : cachedFilters) {
                    if (!filter.getPredicate().apply(foo)) {
                        it.remove();
                        changed = true;
                    }
                }
            }
        }
        if (changed) {
            notifyDataSetChanged();
        }
    }

    public void add(T item) {
        if (!itemList.contains(item)) {
            itemList.add(item);
        } else {
            itemList.remove(item);
            itemList.add(item);
        }
        notifyDataSetChanged();
    }

    public void addAll(Collection<T> newItems) {
        itemList.addAll(newItems);
        notifyDataSetChanged();
    }

    @SuppressWarnings("unused")
    public void onEvent(FilterEvent<T> event) {
        switch (event.getAction()) {
            case ADD:
                // The predicate might have changed so remove existing
                cachedFilters.remove(event.getFilter());
                cachedFilters.add(event.getFilter());
                break;
            case REMOVE:
                cachedFilters.remove(event.getFilter());
                break;
        }
        applyFilters();
    }

    @SuppressWarnings("unused,unchecked")
    public void onEvent(SortEvent event) {
        sort(event.getComparator());
    }

    @SuppressWarnings("unused,unchecked")
    public void onEvent(ChangeEvent event) {
        T entity = (T) event.getEntity();
        if (event.isNewItem()) {
            itemList.add(entity);
        }
        notifyDataSetChanged();
    }


    protected void onClick(Class newView, String dataKey, Serializable data) {
        post(new LoadViewEvent(newView, PlatformUtil.bundleSerializable(dataKey, data)));
    }

    protected static void post(Object event) {
        EventBus.getDefault().post(event);
    }

    protected String getCacheFilterKey() {
        return "";
    }

    public void clear() {
        itemList.clear();
    }

    public void remove(T toSave) {
        itemList.remove(toSave);
        notifyDataSetChanged();
    }

    public enum FilterAction {ADD, REMOVE}

    public static class FilterEvent<T> {

        private final Filter<T> filter;
        private final FilterAction action;

        public FilterEvent(Filter<T> filter, FilterAction action) {
            this.filter = filter;
            this.action = action;
        }

        public Filter<T> getFilter() {
            return filter;
        }

        public FilterAction getAction() {
            return action;
        }
    }

    public static class SortEvent<T> {
        private final Comparator<T> comparator;

        public SortEvent(Comparator<T> comparator) {
            this.comparator = comparator;
        }

        public Comparator<T> getComparator() {
            return comparator;
        }

    }

    public static class ChangeEvent<K> {
        private final K entity;
        private final boolean newItem;

        public ChangeEvent(K entity, boolean newItem) {
            this.entity = entity;
            this.newItem = newItem;
        }

        public K getEntity() {
            return entity;
        }

        public boolean isNewItem() {
            return newItem;
        }
    }

    public static abstract class ViewHolder<T> extends RecyclerView.ViewHolder {

        protected Context context;
        protected ImageView ivEdit, ivDelete;

        public ViewHolder(View v) {
            super(v);
        }

        public ViewHolder(View v, Context context) {
            super(v);
            this.context = context;
        }

        public void initView(final T item) {
            if (ivEdit != null) {
                ivEdit.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                editClicked(v, item);
                            }
                        }
                );
            }
            if (ivEdit != null) {
                ivDelete.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                deleteClicked(v, item);
                            }
                        }
                );
            }
        }

        protected void startNewActivity(Class<? extends Activity> clazz, Bundle data) {
            Intent intent = new Intent(context, clazz);
            intent.putExtras(data);
            context.startActivity(intent);
        }

        protected void deleteClicked(View v, T product) {
            animate(v);
            post(new DeleteClickedEvent<>(product));
        }

        protected void editClicked(View v, T product) {
            animate(v);
        }

        protected void animate(View v) {
        }

    }
    public void refreshWithSearchResults(List<T> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    private void applyAndAnimateRemovals(List<T> newModels) {
        for (int i = itemList.size() - 1; i >= 0; i--) {
            final T model = itemList.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<T> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final T model = newModels.get(i);
            if (!itemList.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<T> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final T model = newModels.get(toPosition);
            final int fromPosition = itemList.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public T removeItem(int position) {
        final T model = itemList.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, T model) {
        itemList.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final T model = itemList.remove(fromPosition);
        itemList.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    public void reload() {
        new LoadTask(true).execute();
    }

    public List<T> getData(long lastId) {
        return new ArrayList<>();
    }

    private class LoadTask extends AsyncTask<Void, Void, List<T>> {
        private final boolean clearExisting;

        public LoadTask() {
            this.clearExisting = false;
        }

        public LoadTask(boolean clearExisting) {
            this.clearExisting = clearExisting;
        }

        @Override
        protected List<T> doInBackground(Void... params) {
            return getData(-1);
        }

        @Override
        protected void onPostExecute(List<T> ts) {
            if (clearExisting) {
                clear();
                itemList.addAll(ts);
                notifyDataSetChanged();
            } else {
                if (ts != null && ts.size() > 0) {
                    itemList.addAll(ts);
                    notifyDataSetChanged();
                }
            }

        }
    }

    public static class DeleteClickedEvent<T> {
        private final T entity;

        public DeleteClickedEvent(T entity) {
            this.entity = entity;
        }

        public T getEntity() {
            return entity;
        }
    }

    public static class ItemSelectedEvent<T> {
        private final T entity;

        public ItemSelectedEvent(T entity) {
            this.entity = entity;
        }

        public T getEntity() {
            return entity;
        }
    }

}