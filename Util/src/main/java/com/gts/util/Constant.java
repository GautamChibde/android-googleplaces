package com.gts.util;

import java.text.SimpleDateFormat;
import java.util.Locale;

public interface Constant {

    String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss.SSSSSS";
    SimpleDateFormat timeFormatter =
            new SimpleDateFormat("HH:mm", Locale.getDefault());
    SimpleDateFormat defaultDateFormatter =
            new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

    String PICKER_LISTENER = "PICKER_LISTENER";
    String MODEL = "MODEL";

    String KEY_USER_TOKEN = "KEY_USER_TOKEN";
    String KEY_CLIENT_TOKEN = "KEY_CLIENT_TOKEN";
    String KEY_USER_ROL = "KEY_USER_ROL";
    String KEY_USER_ID = "KEY_USER_ID";

    String CONTEXT_SIGNUP = "/api/signup";
    String CONTEXT_TOKEN = "/api/token";

    String GRANT_TYPE_PASSWORD = "password";
    String GRANT_TYPE_CLIENT_CREDENTIALS = "client_credentials";

    String FOR_RESULT = "FOR_RESULT";
    String REQUEST_CODE = "REQUEST_CODE";

    int TOKEN_REQUEST_CODE = 101;
    int AUTH_REQUEST_CODE = 102;
    int PLACE_ORDER_REQUEST_CODE = 104;

    String SEARCH_RESOURCE_URI = "/api/search";

    String LISTENER = "LISTENER";
    String MESSAGE = "MESSAGE";

    String THEME = "THEME";

    String HOUR = "HOUR";
    String MINUTE = "MINUTE";

    String YT_KEY = "YOUR_KEY";
}
