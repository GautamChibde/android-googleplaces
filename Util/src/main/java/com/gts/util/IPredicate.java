package com.gts.util;

public interface IPredicate<T> {
    boolean apply(T type);
}
